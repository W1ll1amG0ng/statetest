#include "statetest_handle.h"

int main(int argc, char **argv) {

    ros::init(argc, argv, "statetest");
    StateTestHandle handle = StateTestHandle();
    handle.start();
    //ros::NodeHandle nh;

    while(ros::ok()) {
        ros::spinOnce();
    }
    return 0;

}
#include "statetest_handle.h"

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseStamped.h>

StateTestHandle::StateTestHandle() : node_("~") {
    if (!node_.param<std::string>("statetest/state_topic", state_topic_, "/estimation/slam/state")) {
        ROS_WARN_STREAM("Did not load statetest/state_topic. Standard value is: " << state_topic_);
    }
    if (!node_.param<std::string>("statetest/path_topic", path_topic_, "/statetest/state_path")) {
        ROS_WARN_STREAM("Did not load mapping/path_topic. Standard value is: " << path_topic_);
    }
    if (!node_.param<std::string>("statetest/state_to_rqt_topic", state_to_rqt_topic_, "/statetest/state_to_rqt")) {
        ROS_WARN_STREAM("Did not load mapping/state_to_rqt_topic. Standard value is: " << state_to_rqt_topic_);
    }
    if (!node_.param<std::string>("statetest/Visualization_frame_id", Visualization_frame_id_, "world")) {
        ROS_WARN_STREAM("Did not load mapping/Visualization_frame_id. Standard value is: " << Visualization_frame_id_);
    }
    if (!node_.param<int>("statetest/state_sampling_spacing", state_sampling_spacing_, 1)) {
        ROS_WARN_STREAM("Did not load mapping/state_sampling_spacing. Standard value is: " << state_sampling_spacing_);
    }

    state_num_ = 0;
    current_state_id_ = 0;
    path_.header.stamp = ros::Time::now();
    path_.header.frame_id = Visualization_frame_id_;
}

void StateTestHandle::start() {
    state_sub = node_.subscribe<fsd_common_msgs::CarState>(state_topic_, 1, &StateTestHandle::stateCallback, this);
    map_markerPub_ = node_.advertise<visualization_msgs::MarkerArray>("/map/visualization",1,true);
    path_pub_ = node_.advertise<nav_msgs::Path>(path_topic_, 1, true);
    state_to_rqt_pub_ = node_.advertise<statetest::OriginState>(state_to_rqt_topic_, 1, true);
    std::cout << "Start! " << std::endl;
}

void StateTestHandle::stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg) {

    if(current_state_id_ % state_sampling_spacing_ == 0) {

        ros::Time start_time = ros::Time::now();

        statetest::OriginState state_to_rqt;

        ROS_INFO("x: %f, y: %f", msg->car_state.x, msg->car_state.y);
        state_x_[state_num_%MAX_STATE_NUM] = msg->car_state.x;
        state_y_[state_num_%MAX_STATE_NUM] = msg->car_state.y;

        state_to_rqt.x = msg->car_state.x;
        state_to_rqt.y = msg->car_state.y;

        ROS_INFO("State num: %d", state_num_);
        state_num_++;

        if(msg->header.frame_id != "world") return;

        map_sendVisualization();
        updatePath(*msg);
        path_pub_.publish(path_);
        state_to_rqt_pub_.publish(state_to_rqt);

    }

    current_state_id_++;
}

void StateTestHandle::map_sendVisualization()
{
  visualization_msgs::Marker cone_marker;
  visualization_msgs::MarkerArray cone_maker_array;
  //Set the marker type
  cone_marker.type = visualization_msgs::Marker::CUBE;
  cone_marker.action = visualization_msgs::Marker::ADD;

  cone_marker.header.stamp = ros::Time::now();
  cone_marker.header.frame_id = Visualization_frame_id_; 
														   
  cone_marker.ns = "statetest";

  cone_marker.lifetime = ros::Duration();
  cone_marker.frame_locked = true;
  //Set the color
  cone_marker.color.r = 1.0f;
  cone_marker.color.g = 0.0f;
  cone_marker.color.b = 0.0f;
  cone_marker.color.a = 0.8f;

  cone_marker.scale.x = 0.2;
  cone_marker.scale.y = 0.2;
  cone_marker.scale.z = 0.3;

  for(int i = 0; i < state_num_; i++)
  {
    cone_marker.id++;
    cone_marker.pose.position.x = state_x_[i];
    cone_marker.pose.position.y = state_y_[i];
    
    cone_marker.color.r = 0.0f;
    cone_marker.color.g = 1.0f;
    cone_marker.color.b = 0.0f;

    cone_maker_array.markers.push_back(cone_marker);
  }
  ROS_INFO("cone_marker.id: %d", cone_marker.id);
  map_markerPub_.publish(cone_maker_array);

}

void StateTestHandle::updatePath(const fsd_common_msgs::CarState state) {

    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.stamp = ros::Time::now();
    pose_stamped.header.frame_id = Visualization_frame_id_;
    pose_stamped.pose.position.x = state.car_state.x;
    pose_stamped.pose.position.y = state.car_state.y;
    pose_stamped.pose.position.z = 0.0;
    pose_stamped.pose.orientation.w = 1.0;

    path_.poses.push_back(pose_stamped);

}
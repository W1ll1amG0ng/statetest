#pragma once

#include "fsd_common_msgs/CarState.h"
#include "statetest/OriginState.h"

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <string>
#include <vector>

const int MAX_STATE_NUM = 10000;

class StateTestHandle {
    public:
        StateTestHandle();
        void start();
        void stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg);
        void map_sendVisualization();
        void updatePath(const fsd_common_msgs::CarState state);

    private:
        ros::NodeHandle node_;
        ros::Subscriber state_sub;
        std::string state_topic_;
        std::string path_topic_;
        std::string state_to_rqt_topic_;
        ros::Publisher map_markerPub_;
        ros::Publisher path_pub_;
        ros::Publisher state_to_rqt_pub_;
        std::string Visualization_frame_id_;
        nav_msgs::Path path_;
        int state_sampling_spacing_;
        int current_state_id_;

        int state_num_;
        double state_x_[MAX_STATE_NUM];
        double state_y_[MAX_STATE_NUM];
};